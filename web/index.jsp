<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; UTF-8">
  <link href="css/style.css" rel="stylesheet">
  <script src="http://code.jquery.com/jquery-2.2.4.js"
          type="text/javascript"></script>
  <script src="js/progress.js" type="text/javascript"></script>
  <title>Progress bar</title>
</head>
<body onload="startProgress()">
<p />

<progress id="progressBar" value="0" max="100"></progress>
<span id="progressId" class="progress">0%</span>
</body>
</html>